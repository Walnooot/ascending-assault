﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {
	public float Health, MaxHealth, HealSpeed;
	public int NumGuns;

	public float NeutralTime, ProduceTime, TimeBetweenShots, Cooldown;

	public EnemyState State;

	public GameObject Bullet;

	public Text StatusLabel;

	private float neutralTimer, produceTimer, fireTimer, healCooldown;
	private int numberShots;

	void Start () {
		
	}
	
	void Update () {
		string status = "";

		switch (State) {
			case EnemyState.Neutral:
				neutralTimer += Time.deltaTime;

				if (neutralTimer >= NeutralTime || Health < MaxHealth) {
					State = EnemyState.Producing;
				}

				status = "Neutral.";
				break;
			case EnemyState.Rebuilding:
				Health += Time.deltaTime * HealSpeed;
				Health = Mathf.Clamp(Health, 0f, MaxHealth);
				neutralTimer = NeutralTime;

				healCooldown -= Time.deltaTime;
				if (healCooldown <= 0f || Health >= MaxHealth) {
					State = EnemyState.Producing;
					healCooldown = Cooldown;
				}

				status = "Rebuilding city.";
				break;
			case EnemyState.Producing:
				produceTimer += Time.deltaTime;

				healCooldown -= Time.deltaTime;
				if (Health < MaxHealth && healCooldown <= 0f) {
					healCooldown = Cooldown;
					State = EnemyState.Rebuilding;
				}

				if (produceTimer > ProduceTime) {
					produceTimer = 0f;
					State = EnemyState.Firing;
				}

				status = "Producing ammunition (" + 10 * (int)(10 * produceTimer / ProduceTime) + "%).";
				break;
			case EnemyState.Firing:
				fireTimer += Time.deltaTime;
				if (fireTimer > TimeBetweenShots) {
					fireTimer -= TimeBetweenShots;

					fire();

					numberShots++;

					if (numberShots == NumGuns) {
						fireTimer = 0f;
						numberShots = 0;
						State = EnemyState.Producing;
					}
				}

				status = "Firing artillery!";

				break;
		}

		string health = (int)Health + "/" + (int)MaxHealth;
		StatusLabel.text = health + "\n" + status;
	}

	public void Damage (float damage) {
		Health -= damage;
		if (Health < 0f) {
			Destroy(gameObject);
		}
	}

	private void fire () {
		BuildingNode[] nodes = FindObjectsOfType<BuildingNode>();
		if (nodes.Length > 0) {
			BuildingNode node = nodes[Random.Range(0, nodes.Length - 1)];

			GameObject b = Instantiate(Bullet, transform.position, transform.rotation);
			b.GetComponent<Bullet>().Target = node.pos;
		}
	}

	public enum EnemyState {
		Neutral, Rebuilding, Producing, Firing
	}
}

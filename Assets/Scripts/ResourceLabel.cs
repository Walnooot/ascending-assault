﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceLabel : MonoBehaviour {
	public Resource Resource;

	private WorldManagement wm;
	private Text text;

	void Start () {
		wm = FindObjectOfType<WorldManagement>();
		text =  GetComponent<Text>();
	}
	
	void Update () {
		text.text = "" + wm.GetResourceAmount(Resource);
	}
}

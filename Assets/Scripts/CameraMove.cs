﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {
	public float Speed;
	public GameObject RotateOrigin;

	void Update()
	{
		Vector3 delta = new Vector3();
		delta += Input.GetAxisRaw("Horizontal") * transform.right;
		delta.y = 0f;
		delta.Normalize();
		delta += Input.GetAxisRaw("Vertical") * Vector3.up;
		transform.position += delta.normalized * Time.deltaTime * Speed * Camera.main.orthographicSize;

		Camera.main.orthographicSize *= Mathf.Pow(1.1f, -Input.mouseScrollDelta.y);
		Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, 1f, 5f);

		if (Input.GetKeyDown("space")) {
			transform.RotateAround(RotateOrigin.transform.position, Vector3.up, 90f);
		}
	}
}

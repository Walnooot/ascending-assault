﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldManagement : MonoBehaviour {
	private List<BuildingNode> buildings = new List<BuildingNode>();

	public int Width, Height;

	public float ArtilleryReload;

	private Dictionary<Resource, int> resources = new Dictionary<Resource, int>();

	private bool firing;
	private float fireTimer;
	private int fireIndex;

	private float artilleryTimer;

	void Start () {
		AddResource(Resource.Steel, 300);

		artilleryTimer = ArtilleryReload;
	}

	void Update () {
		if (firing) {
			fireTimer += Time.deltaTime;
			if (fireTimer > 1f) {
				fireTimer -= 1f;

				List<Gun> guns = new List<Gun>();
				foreach (Gun a in FindObjectsOfType<Gun>()) {
					if (a.GetComponent<Agent>().Finished) {
						guns.Add(a);
					}
				}

				if (fireIndex >= guns.Count) {
					firing = false;
					fireIndex = 0;
					fireTimer = 0f;
				} else {
					if (Pay(Resource.Steel, 200)) {
						guns[fireIndex].Fire();
						fireIndex++;
					}
				}
			}
		}

		artilleryTimer += Time.deltaTime;

		if (FindObjectOfType<Enemy>() == null) {
			SceneManager.LoadScene("End");
		}
	}

	private int getID(int x, int y, int z) {
		return y * (Width * Height) + z * Width + x;
	}

	private int getID(BuildingNode node) {
		return getID(node.pos.x, node.pos.y, node.pos.z);
	}

	private BuildingNode search (int x, int y, int z, out int index) {
		if (buildings.Count == 0) {
			index = 0;
			return null;
		}

		int target = getID(x, y, z);
		int begin = 0;
		int end = buildings.Count - 1;

		while (begin != end) {
			int middle = (begin + end) / 2;

			BuildingNode n = buildings[middle];
			int i = getID(n);
			if (i == target) {
				index = i;
				return n;
			} else if (target > i) {
				begin = middle + 1;
			} else {
				end = middle - 1;
			}
		}



		index = begin + 1;
		return null;
	}

	public void RegisterBuilding(BuildingNode node) {
		buildings.Add(node);
	}

	public BuildingNode GetNode (int x, int y, int z) {
		foreach (BuildingNode n in buildings) {
			if (n.pos.x == x && n.pos.y == y && n.pos.z == z) {
				return n;
			}
		}

		return null;
	}

	public BuildingNode GetNode(Vector3Int pos) {
		return GetNode(pos.x, pos.y, pos.z);
	}

	public bool CanPlace (GameObject go) {
		foreach (BuildingNode n in go.GetComponentsInChildren<BuildingNode>(true)) {
			int x = Mathf.RoundToInt(n.transform.position.x);
			int y = Mathf.RoundToInt(n.transform.position.y);
			int z = Mathf.RoundToInt(n.transform.position.z);

			if (GetNode(x, y, z) != null) {
				return false;
			}

			if (x >= Width || z >= Height) {
				return false;
			}
		}

		Agent agent = go.GetComponent<Agent>();
		if (agent != null) {
			foreach (Cost c in agent.Costs) {
				if (GetResourceAmount(c.Resource) < c.Amount) {
					return false;
				}
			}
		}

		return true;
	}

	public bool PlaceBuilding (GameObject building) {
		if (CanPlace(building)) {
			Agent agent = building.GetComponent<Agent>();
			if (agent != null) {
				foreach (Cost c in agent.Costs) {
					AddResource(c.Resource, -c.Amount);
				}

				return true;
			}
		}

		return false;
	}

	public void RemoveNode (BuildingNode node) {
		buildings.Remove(node);
	}

	public void AddResource (Resource resource, int amount) {
		if (!resources.ContainsKey(resource)) {
			resources[resource] = 0;
		}

		resources[resource] += amount;
	}

	public int GetResourceAmount (Resource resource) {
		return resources.ContainsKey(resource) ? resources[resource] : 0;
	}

	public bool Pay (Resource resource, int amount) {
		if (resources[resource] < amount) {
			return false;
		} else {
			resources[resource] -= amount;
			return true;
		}
	}

	public float ReloadProgress () {
		return artilleryTimer / ArtilleryReload;
	}

	public void FireArtillery () {
		if (ReloadProgress() > 1f) {
			firing = true;
			artilleryTimer = 0f;
		}
	}
}

﻿using System;

[System.Serializable]
public class Cost {
	public Resource Resource;
	public int Amount;
}

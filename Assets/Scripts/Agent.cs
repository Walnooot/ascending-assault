﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent : MonoBehaviour {
	public float MaxHealth, Health;

	public bool Finished, IsWorker, IsStoreRoom, Deconstruct, MakesWorkers;

	public string Description;

	public Vector3Int pos;

	public Resource Product;
	public float ProductionTime;

	public List<Cost> Costs;

	public string Name;

	private WorldManagement wm;

	private List<BuildingNode> path = new List<BuildingNode>();
	private float walkTimer = 0f, checkTimer, productionTimer = 0f;

	private bool hasResource;

	private bool makingWorker;
	private float makeWorkerTimer;
	public float MakeWorkerTime;

	public GameObject WorkerPrefab;

	public Resource Carrying;

	public string Status;

	private Vector3 randomOffset;
	private Vector3 targetPos;

	void Start () {
		pos.x = Mathf.RoundToInt(transform.position.x);
		pos.y = Mathf.RoundToInt(transform.position.y);
		pos.z = Mathf.RoundToInt(transform.position.z);

		wm = FindObjectOfType<WorldManagement>();

		randomOffset = new Vector3();
		randomOffset.x = Random.Range(-.1f, .1f);
		randomOffset.z = Random.Range(-.1f, .1f);

		targetPos = transform.position;
	}
	
	void Update () {
		if (IsWorker) {
			if (path.Count > 0) {
				walkTimer += Time.deltaTime;

				Vector3 tpos = path[0].pos - pos;
				tpos *= walkTimer;
				tpos += pos;
				targetPos = tpos;

				if (walkTimer > 1f) {
					walkTimer -= 1f;

					pos = path[0].pos;
					path.RemoveAt(0);
					targetPos = pos;
				}
			} else {
				checkWork();

				walkTimer = 0f;
			}
		} else {
			if (Finished && !hasResource && Product != Resource.None) {
				productionTimer += Time.deltaTime;
				if (productionTimer > ProductionTime) {
					productionTimer = 0f;
					hasResource = true;
				}
			}

			if (makingWorker) {
				makeWorkerTimer += Time.deltaTime;

				if (makeWorkerTimer > MakeWorkerTime) {
					makeWorkerTimer = 0f;
					makingWorker = false;

					bool found = false;
					foreach (BuildingNode bn in FindObjectsOfType<BuildingNode>()) {
						if (found) break;

						if (bn.GetAgent() == this) {
							foreach (BuildingNode road in bn.reachables) {
								if (road.Type == BuildingNode.NodeType.Road) {
									Instantiate(WorkerPrefab, road.pos, Quaternion.identity);

									found = true;
									break;
								}
							}
						}
					}

					if (!found) {
						Debug.Log("Could not find road to place worker");
					}
				}
			}
		}

		if (IsWorker) {
			Vector3 offset = 10f * ((targetPos + randomOffset) - transform.position);
			foreach (Agent a in FindObjectsOfType<Agent>()) {
				if (a != this && a.IsWorker) {
					Vector3 diff = transform.position - a.transform.position;
					float dist = Mathf.Clamp(diff.magnitude * 3f, 0f, 1f);
					offset += 5f * diff * (1f - dist);
				}
			}

			transform.position += offset * Time.deltaTime;
		}
	}

	private bool goesTo(Agent building) {
		if (Carrying != Resource.None && building.IsStoreRoom) {
			return true;
		}

		if (Carrying == Resource.None || !building.Needs(WorkType.Haul)) {
			return building.NeedsWorker();
		}

		return false;
	}

	private bool workerWork(Agent building) {
		if (Carrying != Resource.None) {
			if (building.IsStoreRoom) {
				wm.AddResource(Carrying, 50);
				Carrying = Resource.None;
				return true;
			}
		}

		if (Carrying == Resource.None || !building.Needs(WorkType.Haul)) {
			if (building.Work(this)) {
				return true;
			}
		}

		return false;
	}

	private void checkWork () {
		BuildingNode buildingNode = wm.GetNode(pos);
		if (buildingNode != null) {
			foreach (BuildingNode bn in buildingNode.reachables) {
				Agent a = bn.GetAgent();
				if (a != null) {
					if (workerWork(a)) {
						return;
					}
				}
			}

			checkTimer -= Time.deltaTime;
			if (checkTimer < 0f) {
				HashSet<BuildingNode> goals = new HashSet<BuildingNode>();

				foreach (BuildingNode bn in FindObjectsOfType<BuildingNode>()) {
					Agent agent = bn.GetAgent();
					if (agent != null && goesTo(agent)) {
						foreach(BuildingNode neighbour in bn.reachables) {
							goals.Add(neighbour);
						}
					}
				}

				foreach (Agent a in FindObjectsOfType<Agent>()){
					goals.Remove(a.getGoal());
				}

				if (goals.Count == 0) {
					Status = "Nothing to do";
					checkTimer = 0.5f;
				} else {
					bool success = aStar(buildingNode, goals, null);
					if (!success) {
						// check again in some time
						checkTimer = 0.5f;

						Status = "Can't find path";
					} else {
						// check immediatly after were done walking
						checkTimer = 0f;
					}
				}
			}
		}
	}

	private BuildingNode getGoal () {
		if (path.Count > 0) {
			return path[path.Count - 1];
		} else {
			return null;
		}
	}

	public bool Needs (WorkType type) {
		switch(type) {
			case WorkType.Repair:
				return Deconstruct ? true : Health < MaxHealth;
			case WorkType.Haul:
				return hasResource;
		}

		return false;
	}

	public bool NeedsWorker () {
		return Needs(WorkType.Repair) || Needs(WorkType.Haul);
	}

	public bool Work (Agent worker) {
		if (Deconstruct || Health < MaxHealth) {
			worker.Status = "Repairing";

			changeHealth((Deconstruct ? -3f : 1f) * Time.deltaTime * 40f);
			if (Health >= MaxHealth) {
				Finished = true;
			}

			return true;
		} else if (hasResource) {
			worker.Status = "Picking up resource";
			hasResource = false;
			worker.Carrying = Product;
			return true;
		}

		return false;
	}

	public void Damage (float damage) {
		changeHealth(-damage);
	}

	private void changeHealth (float amount) {
		Health += amount;
		Health = Mathf.Clamp(Health, 0f, MaxHealth);

		if (Health <= 0f) {
			Destroy(gameObject);
		}
	}

	public bool MoveTo(int x, int y, int z) {
		BuildingNode start = wm.GetNode(pos);
		BuildingNode goal = wm.GetNode(x, y, z);

		if (goal == null) {
			Debug.Log("Goal not on a road");
		} else if (start == null) {
			Debug.Log("Player not on a road");
		} else {
			HashSet<BuildingNode> set = new HashSet<BuildingNode>();
			set.Add(goal);
			return aStar(start, set, goal);
		}

		return false;
	}

	public float GetResouceProgress () {
		return productionTimer / ProductionTime;
	}

	public void MakeWorker () {
		if (MakesWorkers && wm.Pay(Resource.Fuel, 160)) {
			makingWorker = true;
		}
	}

	public float WorkerProgress () {
		return makeWorkerTimer / MakeWorkerTime;
	}

	private int heuristic (BuildingNode start, BuildingNode goal) {
		if (goal == null) {
			return 0;
		}

		Vector3Int diff = start.pos - goal.pos;
		return Mathf.Abs(diff.x) + Mathf.Abs(diff.y) + Mathf.Abs(diff.z);
	}

	private bool aStar(BuildingNode start, HashSet<BuildingNode> goals, BuildingNode target) {
		HashSet<BuildingNode> closed = new HashSet<BuildingNode>();
		List<BuildingNode> open = new List<BuildingNode>();
		open.Add(start);

		Dictionary<BuildingNode, BuildingNode> cameFrom = new Dictionary<BuildingNode, BuildingNode>();
		Dictionary<BuildingNode, int> gScore = new Dictionary<BuildingNode, int>();
		gScore.Add(start, 0);
		Dictionary<BuildingNode, int> fScore = new Dictionary<BuildingNode, int>();
		fScore.Add(start, heuristic(start, target));

		while(open.Count > 0) {
			BuildingNode current = null;
			int lowestF = int.MaxValue;
			foreach (BuildingNode bn in open) {
				int f = int.MaxValue;
				fScore.TryGetValue(bn, out f);

				if (current == null || f < lowestF) {
					current = bn;
					lowestF = f;
				}
			}

			if (goals.Contains(current)) {
				buildPath(current, start, cameFrom);
				return true;
			}

			open.Remove(current);
			closed.Add(current);

			foreach (BuildingNode bn in current.GetReachables())
			{
				if (closed.Contains(bn))
				{
					continue;
				}

				if (bn.Type != BuildingNode.NodeType.Building) {
					int gCurrent = int.MaxValue;
					gScore.TryGetValue(current, out gCurrent);

					int gNeighbour = int.MaxValue;
					gScore.TryGetValue(bn, out gNeighbour);

					int tentativeGScore = gCurrent + 1;

					if (!open.Contains(bn)) {
						open.Add(bn);
					} else if (tentativeGScore >= gNeighbour) {
						continue;
					}

					cameFrom.Add(bn, current);
					gScore.Add(bn, tentativeGScore);
					fScore.Add(bn, tentativeGScore + heuristic(bn, target));
				}
			}
		}

		return false;
	}

	private void buildPath(BuildingNode goal, BuildingNode start, Dictionary<BuildingNode, BuildingNode> cameFrom)
	{
		path.Clear();

		BuildingNode current = goal;
		while (current != null && current != start) {
			path.Add(current);

			current = cameFrom.ContainsKey(current) ? cameFrom[current] : null;
		}

		path.Reverse();
	}

	public enum WorkType {
		Repair, Haul
	}
}

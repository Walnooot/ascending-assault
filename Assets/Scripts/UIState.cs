﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIState : MonoBehaviour {
	public Text PanelTitle, PanelDescription;
	public GameObject Panel;

	public Material ErrorMaterial;

	public AudioSource PlaceSound;

	public LayerMask PlaceMask, SelectMask;

	public GameObject Grid;

	public MenuElement MakeWorkerButton, DeconstructButton;

	private MenuState state;
	private Agent agent;
	private WorldManagement wm;

	void Start () {
		state = MenuState.Empty;
		SetMenuState(MenuState.Help);

		wm = FindObjectOfType<WorldManagement>();
	}
	
	void Update () {
		if (Input.GetKeyDown("escape")) {
			SetMenuState(MenuState.Empty);
		}

		if (state == MenuState.Agent) {
			if (agent == null) {
				SetMenuState(MenuState.Empty);
			} else {
				if (agent.IsWorker && Input.GetMouseButtonDown(1)) {
					Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;

					if (Physics.Raycast(r, out hit, 1000f, SelectMask)) {
						agent.MoveTo(Mathf.RoundToInt(hit.point.x), Mathf.RoundToInt(hit.point.y), Mathf.RoundToInt(hit.point.z));
					}
				}
			}
		}

		if (state == MenuState.Build)
		{
			if (Input.GetMouseButtonDown(1))
			{
				foreach (BuildingPlacement bp in Object.FindObjectsOfType<BuildingPlacement>())
				{
					Destroy(bp.gameObject);
				}
			}

			if (Input.GetButtonDown("up")) {
				Grid.transform.position += Vector3.up;
			}

			if (Input.GetButtonDown("down") && Grid.transform.position.y > 0.5f) {
				Grid.transform.position -= Vector3.up;
			}
		}

		updatePanel();
	}

	public void Place (GameObject gameObject) {
		foreach (BuildingPlacement bp in Object.FindObjectsOfType<BuildingPlacement>()) {
			Destroy(bp.gameObject);
		}

		GameObject instance =  Instantiate(gameObject);
		BuildingPlacement newbp = (BuildingPlacement) instance.AddComponent(typeof(BuildingPlacement));
		newbp.ErrorMaterial = ErrorMaterial;
		newbp.PlaceSound = PlaceSound;
		newbp.Mask = PlaceMask;
	}

	public void Click () {
		bool isPlacing = false;
		foreach (BuildingPlacement bp in Object.FindObjectsOfType<BuildingPlacement>()) {
			bp.Click();
			isPlacing = true;
		}

		if (!isPlacing) {
			Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			bool foundAgent = false;

			if (Physics.Raycast(r, out hit))
			{
				GameObject go = hit.collider.gameObject;
				Agent agent = go.GetComponent<Agent>();

				if (agent == null && go.transform.parent != null) {
					agent = go.transform.parent.GetComponent<Agent>();
				}

				if (agent != null)
				{
					this.agent = agent;
					SetMenuState(MenuState.Agent, false);
					foundAgent = true;
				}
			}

			if(!foundAgent) {
				SetMenuState(MenuState.Empty);
			}
		}
	}

	private void updatePanel () {
		string title = null;
		string description = null;

		if (state == MenuState.Build) {
			title = "Construct Buildings";
			description = "";

			BuildingPlacement bp =  FindObjectOfType<BuildingPlacement>();
			if (bp != null) {
				Agent a = bp.GetComponent<Agent>();

				title = a.Name;

				description += a.Description + "\n";

				foreach(Cost c in a.Costs) {
					description += System.Enum.GetName(typeof(Resource), c.Resource) + ": " + c.Amount + "\n";
				}
			} else {
				description += "Place buildings in the world. Place with left click, rotate with Q and E. Move up and down with R and F. Cancel with right click. ";
			}
		} else if (state == MenuState.Artillery) {
			title = "Artillery";

			if (FindObjectOfType<Gun>() == null) {
				description = "Inactive: no guns built.\n";
			} else {
				description = "Order an artillery strike. Costs 200 steel per gun.";

				float progress = wm.ReloadProgress();
				if (progress >= 1f) {
					description += "\n\nReady.";
				} else  {
					description += "\n\nReloading: " + (int) (100 * progress) + "%.";
				}
			}
		} else if (state == MenuState.Help) {
			title = "Help";
			description = "Move the camera with WASD. ";
			description += "Select units with left click. ";
			description += "Quit menus with escape. \n";
			description += "Start with selecting 'Build', and building a Steel Mill and a Refinery. ";
		} else if (state == MenuState.Agent) {
			title = agent.Name;
			description = "Health: " + (int) agent.Health + "/" + (int) agent.MaxHealth;
			if (agent.IsWorker) {
				description += "\nRight click to move.";
			}

			if (agent.WorkerProgress() > 0f) {
				description += "\nWorker progress: " + (int) (100 * agent.WorkerProgress()) + "%.";
			}

			if (agent.Carrying != Resource.None) {
				description += "\nCarrying " + System.Enum.GetName(typeof(Resource), agent.Carrying) + ".";
			}

			if (agent.Needs(Agent.WorkType.Repair)) {
				description += agent.Finished ? "\nNeeds repair." : 
				                    (agent.Deconstruct ? "\nNeeds deconstructing." : "\nNeeds building.");
			}

			string product = System.Enum.GetName(typeof(Resource), agent.Product);
			if (agent.Needs(Agent.WorkType.Haul)) {
				description += "\n" + product + " ready.";
			} else {
				if (agent.Product != Resource.None) {
					string progress = "" + (int)(agent.GetResouceProgress() * 100);
					description += "\n" + product + " progress: " + progress + "%.";
				}
			}
		}

		Panel.SetActive(title != null);
		PanelTitle.text = title;
		PanelDescription.text = description;
	}

	public void SetMenuState (MenuState newState) {
		SetMenuState(newState, true);
	}

	public void SetMenuState (MenuState newState, bool toggle) {
		if (this.state == MenuState.Build) {
			foreach (BuildingPlacement bp in Object.FindObjectsOfType<BuildingPlacement>()) {
				Destroy(bp.gameObject);
			}
		}

		MenuState state = (toggle && newState == this.state) ? MenuState.Empty : newState;
		this.state = state;

		foreach (MenuElement me in Resources.FindObjectsOfTypeAll(typeof(MenuElement))) {
			if (state == MenuState.Agent) {
				if (me == DeconstructButton) {
					me.gameObject.SetActive(!agent.IsWorker);
				} else if (me == MakeWorkerButton) {
					me.gameObject.SetActive(agent.MakesWorkers);
				} else {
					me.gameObject.SetActive(false);
				}
			} else {
				me.gameObject.SetActive(me.State == state);
			}
		}

		updatePanel();

		Grid.SetActive(state == MenuState.Build);
	}

	public void BuildState () {
		SetMenuState(MenuState.Build);
	}

	public void ResearchState () {
		SetMenuState(MenuState.Artillery);
	}

	public void HelpState () {
		SetMenuState(MenuState.Help);
	}

	public void Deconstruct () {
		agent.Deconstruct = !agent.Deconstruct;
	}

	public void MakeWorker () {
		agent.MakeWorker();
	}

	[System.Serializable]
	public enum MenuState
	{
		Empty, Build, Artillery, Agent, Help
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public Vector3Int Target;
	public float Height, Speed, Damage;

	public Enemy Enemy;

	public AudioSource ExplodeSound;

	private Vector3 origin;
	private WorldManagement wm;
	private float timer;

	void Start () {
		origin = transform.position;

		wm = FindObjectOfType<WorldManagement>();
	}
	
	void Update () {
		timer += Time.deltaTime;

		float dist = (origin - Target).magnitude;
		float totalTime = dist / Speed;
		float progress = timer / totalTime;

		transform.position = origin + (Target - origin) * progress;
		transform.position += Vector3.up * Height * (1f - Mathf.Pow((2 * progress - 1), 2f));

		if (Enemy == null) {
			Vector3Int pos = Vector3Int.RoundToInt(transform.position);
			BuildingNode node = wm.GetNode(pos);
			if (node != null || progress > 1f) {
				for (int x = -1; x <= 1; x++) {
					for (int y = -1; y <= 1; y++) {
						for (int z = -1; z <= 1; z++) {
							BuildingNode hit = wm.GetNode(pos + new Vector3Int(x, y, z));
							if (hit != null && hit.GetAgent() != null) {
								hit.GetAgent().Damage(Damage);
							}
						}
					}
				}

				Destroy(gameObject, 5f);
				Destroy(GetComponent<MeshRenderer>());
				Destroy(this);
				GetComponent<AudioSource>().Play();
			}
		} else {
			if (progress > 1f) {
				Enemy.Damage(1500f);

				Destroy(gameObject, 5f);
				Destroy(GetComponent<MeshRenderer>());
				Destroy(this);
				GetComponent<AudioSource>().Play();
			}
		}
	}
}

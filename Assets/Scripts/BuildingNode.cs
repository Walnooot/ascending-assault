﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingNode : MonoBehaviour
{
	public Vector3Int pos;
	public NodeType Type;

	public GameObject ConstructionSprite;

	public Sprite Repair, Haul;

	public HashSet<BuildingNode> reachables = new HashSet<BuildingNode>();

	private Agent agent;

	private SpriteRenderer sprite;
	private WorldManagement wm;

	void Start () {
		pos.x = Mathf.RoundToInt(transform.position.x);
		pos.y = Mathf.RoundToInt(transform.position.y);
		pos.z = Mathf.RoundToInt(transform.position.z);

		wm = FindObjectOfType<WorldManagement>();
		wm.RegisterBuilding(this);

		Vector3Int fwd = new Vector3Int(1, 0, 0);
		Vector3Int right = new Vector3Int(0, 0, 1);

		check(wm.GetNode(pos - fwd));
		check(wm.GetNode(pos + fwd));
		check(wm.GetNode(pos - right));
		check(wm.GetNode(pos + right));

		agent = transform.parent.gameObject.GetComponent<Agent>();

		if (agent != null && ConstructionSprite != null) {
			GameObject go = Instantiate(ConstructionSprite, transform.position, transform.rotation);
			go.transform.parent = transform;
			sprite = go.GetComponent<SpriteRenderer>();
		}

		foreach(BuildingNode bn in FindObjectsOfType<BuildingNode>()) {
			if (bn.agent == agent && bn != this) {
				if (Type == NodeType.Ramp && bn.Type == NodeType.Ramp) {
					MarkReachable(bn);
					bn.MarkReachable(this);

					Debug.Log("Found other ramp piece");
				}

				if (bn.Type != NodeType.Road && bn.pos.y == this.pos.y) {
					MarkReachable(bn);
					bn.MarkReachable(this);

					Debug.Log("found platform piece");
				}
			}
		}
	}

	void Update () {
		if (!agent.NeedsWorker()) {
			sprite.color = Color.clear;
		} else if (sprite != null) {
			if (agent.Needs(Agent.WorkType.Repair)) {
				sprite.sprite = Repair;
			} else if (agent.Needs(Agent.WorkType.Haul)) {
				sprite.sprite = Haul;
			}

			bool blink = !agent.NeedsWorker() || (agent.Health % 10f < 5f);
			sprite.color = blink ? Color.white : Color.gray;
		}
	}

	private void check (BuildingNode node) {
		if (node != null && Type != NodeType.Ramp && node.Type != NodeType.Ramp) {
			node.MarkReachable(this);
			MarkReachable(node);
		}
	}

	public void MarkReachable(BuildingNode node)
	{
		reachables.Add(node);
	}

	public void UnmarkReachable(BuildingNode node)
	{
		reachables.Remove(node);
	}

	public HashSet<BuildingNode> GetReachables () {
		return reachables;
	}

	public Agent GetAgent () {
		return agent;
	}

	void OnDestroy () {
		foreach (BuildingNode bn in reachables) {
			bn.UnmarkReachable(this);
		}

		wm.RemoveNode(this);
	}

	[System.Serializable]
	public enum NodeType
	{
		Building, Road, Ramp
	}
}

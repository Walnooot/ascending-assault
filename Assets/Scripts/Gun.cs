﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {
	public GameObject Bullet;

	public void Fire () {
		Enemy enemy = FindObjectOfType<Enemy>();
		if (enemy != null) {
			Bullet bullet = Instantiate(Bullet, transform.position, transform.rotation).GetComponent<Bullet>();
			bullet.Enemy = enemy;
			bullet.Target = Vector3Int.RoundToInt(enemy.transform.position);
		}
	}
}

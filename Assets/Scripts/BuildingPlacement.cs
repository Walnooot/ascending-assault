﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingPlacement : MonoBehaviour {
	public LayerMask Mask;

	public Material ErrorMaterial;

	private WorldManagement wm;
	private MeshRenderer[] meshes;

	private List<Material[]> oldMaterials = new List<Material[]>();

	public AudioSource PlaceSound;

	void Start () {
		wm = Object.FindObjectOfType<WorldManagement>();

		meshes = GetComponentsInChildren<MeshRenderer>(true);
		foreach(MeshRenderer m in meshes) {
			oldMaterials.Add(m.materials);
		}

		update(false);
	}
	
	void Update () {
		update(false);
	}

	private void update (bool click) {
		if (GetComponent<Gun>() == null) {
			if (Input.GetButtonDown("right")) {
				transform.Rotate(Vector3.up, 90f);
			}

			if (Input.GetButtonDown("left")) {
				transform.Rotate(Vector3.up, -90f);
			}
		}

		Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);

		RaycastHit hit;

		bool canPlace = false;
		bool hasHit = Physics.Raycast(r, out hit, 1000f, Mask);

		if (hasHit)
		{
			Vector3 pos;
			pos.x = Mathf.Round(hit.point.x);
			pos.y = Mathf.Round(hit.point.y);
			pos.z = Mathf.Round(hit.point.z);
			transform.position = pos;

			if (wm.CanPlace(gameObject))
			{
				canPlace = true;

				if (click && wm.PlaceBuilding(gameObject)) {
					GameObject g = Instantiate(gameObject, transform.position, transform.rotation);
					Destroy(g.GetComponent<BuildingPlacement>());

					foreach (BuildingNode n in g.GetComponentsInChildren<BuildingNode>(true))
					{
						n.gameObject.SetActive(true);
					}

					PlaceSound.Play();
				}
			}
			else
			{
				transform.position += Vector3.up * .2f + Vector3.one * -0.001f;
			}
		}

		for (int i = 0; i < meshes.Length; i++) {
			MeshRenderer mr = meshes[i];

			mr.enabled = hasHit;
			if (canPlace) {
				mr.materials = oldMaterials[i];
			} else {
				mr.material = ErrorMaterial;
			}
		}
	}

	public void Click () {
		update(true);
	}
}

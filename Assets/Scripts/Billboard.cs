﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {
	void Start () {
		transform.position -= Camera.main.transform.forward * 5f;
		transform.forward = Camera.main.transform.forward;
		GetComponent<SpriteRenderer>().sortingOrder = 10;
	}
}
